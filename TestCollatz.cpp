// ---------------
// TestCollatz.c++
// ---------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------

#include <sstream>  // istringtstream, ostringstream
#include <tuple>    // make_tuple, tuple
#include <utility>  // make_pair, pair

#include "gtest/gtest.h"

#include "Collatz.hpp"

using namespace std;

// ----
// read
// ----

TEST(CollatzFixture, read) {
    ASSERT_EQ(collatz_read("1 10\n"), make_pair(1, 10));
}

// ----
// eval
// ----

TEST(CollatzFixture, eval0) {
    ASSERT_EQ(collatz_eval(make_pair(1, 10)), make_tuple(1, 10, 20));
}

TEST(CollatzFixture, eval1) {
    ASSERT_EQ(collatz_eval(make_pair(100, 200)), make_tuple(100, 200, 125));
}

TEST(CollatzFixture, eval2) {
    ASSERT_EQ(collatz_eval(make_pair(201, 210)), make_tuple(201, 210, 89));
}

TEST(CollatzFixture, eval3) {
    ASSERT_EQ(collatz_eval(make_pair(900, 1000)), make_tuple(900, 1000, 174));
}

// -----
// print
// -----

TEST(CollatzFixture, print) {
    ostringstream sout;
    collatz_print(sout, make_tuple(1, 10, 20));
    ASSERT_EQ(sout.str(), "1 10 20\n");
}

// -----
// solve
// -----

TEST(CollatzFixture, solve) {
    istringstream sin("1 10\n100 200\n201 210\n900 1000\n");
    ostringstream sout;
    collatz_solve(sin, sout);
    ASSERT_EQ(sout.str(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n");
}

// ------
// answer
// ------

TEST(CollatzFixture, answer1) {
    ASSERT_EQ(collatz_answer(403983, 294293), 441);
}

TEST(CollatzFixture, answer2) {
    ASSERT_EQ(collatz_answer(522063, 860951), 525);
}

TEST(CollatzFixture, answer3) {
    ASSERT_EQ(collatz_answer(925795, 873707), 476);
}

TEST(CollatzFixture, answer4) {
    ASSERT_EQ(collatz_answer(509223, 541032), 470);
}

TEST(CollatzFixture, answer5) {
    ASSERT_EQ(collatz_answer(196338, 643979), 509);
}

TEST(CollatzFixture, answer6) {
    ASSERT_EQ(collatz_answer(457304, 84294), 449);
}

TEST(CollatzFixture, answer7) {
    ASSERT_EQ(collatz_answer(735533, 809001), 468);
}

TEST(CollatzFixture, answer8) {
    ASSERT_EQ(collatz_answer(185906, 587038), 470);
}

TEST(CollatzFixture, answer9) {
    ASSERT_EQ(collatz_answer(284192, 369732), 441);
}

TEST(CollatzFixture, answer10) {
    ASSERT_EQ(collatz_answer(638281, 606439), 509);
}

TEST(CollatzFixture, answer11) {
    ASSERT_EQ(collatz_answer(507662, 847207), 525);
}

TEST(CollatzFixture, answer12) {
    ASSERT_EQ(collatz_answer(629500, 99931), 509);
}

TEST(CollatzFixture, answer13) {
    ASSERT_EQ(collatz_answer(493129, 937806), 525);
}
