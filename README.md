# CS371p: Object-Oriented Programming Collatz Repo

* Name: Safin Kasturi

* EID: ssk2386

* GitLab ID: safinkasturi

* HackerRank ID: safinkasturi2000

* Git SHA: e8c72092945b1d9b3e84d8d953da131331d968c0

* GitLab Pipelines: https://gitlab.com/safinkasturi/cs371p-collatz/-/pipelines

* Estimated completion time: 4 hours

* Actual completion time: 10 hours

* Comments: This project took me a lot longer than expected. I initially thought it would not take me long since I did the same project in Python last semester for SWE, so all I'd have to do is change my Python code to C++. However, I kept running into seg faults that I struggled to resolve, so this drastically increased my completion time.
